/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ClickLog', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    put_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    from_ip: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    from_url: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    click_url: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    visitor: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'ads_click_log'
  });
};
