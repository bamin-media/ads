Finers 广告系统 - 广告管理
===============

当前最新版本： 1.0.0（发布日期：2020-06-21）


[![AUR](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg)]()
[![](https://img.shields.io/badge/Author-FINERS团队-orange.svg)]()
[![](https://img.shields.io/badge/version-1.0.0-brightgreen.svg)]()


介绍
-----------------------------------
Finers广告系统【完全开源】是一款网页广告管理及投放的产品，可作为公共平台为一个或多个网站提供广告服务。 
系统分为广告管理和广告投放两部分，本部分为其中的广告管理。
- 包括广告位管理、广告（物料）管理、投放管理及日志管理等功能； 
- 基于J2EE快速开发平台Jeecg-Boot开发，采用前后端分离架构：SpringBoot2.x，Ant Design&Vue，Mybatis-plus，Shiro，JWT； 
在Jeecg-Boot支撑下，无需写任何代码就可以快速实现大多常用功能，也可手工加入复杂的业务逻辑，从而可以方便快速地进行二次开发！

功能模块
-----------------------------------
```
┌─广告管理
│  ├─投放频道
│  ├─广告位常用尺寸
│  ├─广告位
│  ├─广告
│  ├─投放管理
│  └─投放效果
│     ├─呈现日志
│     └─点击日志
├─系统管理（Jeecg-Boot功能）
│  ├─用户管理
│  ├─角色管理
│  ├─菜单管理
│  ├─权限设置（支持按钮权限、数据权限）
│  ├─表单权限（控制字段禁用、隐藏）
│  ├─部门管理
│  ├─字典管理
│  ├─系统公告
│  ├─我的组织机构
│  ├─职务管理
│  └─通讯录
├─消息中心（Jeecg-Boot）
│  ├─消息管理
│  └─模板管理
├─智能化开发支持（Jeecg-Boot）
│  ├─代码生成器功能（一键生成前后端代码，生成后无需修改直接用，绝对是后端开发福音）
│  ├─代码生成器模板（提供4套模板，分别支持单表和一对多模型，不同风格选择）
│  ├─代码生成器模板（生成代码，自带excel导入导出）
│  ├─查询过滤器（查询逻辑无需编码，系统根据页面配置自动生成）
│  ├─高级查询器（弹窗自动组合查询条件）
│  ├─Excel导入导出工具集成（支持单表，一对多 导入导出）
│  └─平台移动自适应支持
└─系统监控（Jeecg-Boot）
   ├─性能扫描监控
   │  ├─监控 Redis
   │  ├─Tomcat
   │  ├─jvm
   │  ├─服务器信息
   │  ├─请求追踪
   │  └─磁盘监控
   ├─定时任务
   ├─系统日志
   ├─消息中心（支持短信、邮件、微信推送等等）
   ├─数据日志（记录数据快照，可对比快照，查看数据变更情况）
   ├─系统通知
   ├─SQL监控
   └─swagger-ui(在线接口文档)
```
   

 
技术架构
-----------------------------------
#### 开发环境

- 语言：Java 8

- IDE(JAVA)： IDEA / Eclipse，安装lombok插件
 
- IDE(前端)：  IDEA / WebStorm

- 依赖管理：Maven

- 数据库：MySQL5.7+  &  Oracle 11g & SqlServer2017

- 缓存：Redis


#### 后端
- 基础框架：Spring Boot 2.1.3.RELEASE

- 持久层框架：Mybatis-plus_3.1.2

- 安全框架：Apache Shiro 1.4.0，Jwt_3.7.0

- 数据库连接池：阿里巴巴Druid 1.1.10

- 缓存框架：redis

- 日志打印：logback

- 其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。


#### 前端
 
- [Vue 2.6.10](https://cn.vuejs.org/),[Vuex](https://vuex.vuejs.org/zh/),[Vue Router](https://router.vuejs.org/zh/)
- [Axios](https://github.com/axios/axios)
- [ant-design-vue](https://vuecomponent.github.io/ant-design-vue/docs/vue/introduce-cn/)
- [webpack](https://www.webpackjs.com/),[yarn](https://yarnpkg.com/zh-Hans/)
- [vue-cropper](https://github.com/xyxiao001/vue-cropper) - 头像裁剪组件
- [@antv/g2](https://antv.alipay.com/zh-cn/index.html) - Alipay AntV 数据可视化图表
- [Viser-vue](https://viserjs.github.io/docs.html#/viser/guide/installation)  - antv/g2 封装实现
- eslint，[@vue/cli 3.2.1](https://cli.vuejs.org/zh/guide)
- vue-print-nb - 打印



后台开发环境和依赖
----
- java
- maven
- jdk8
- mysql
- redis
- 数据库脚本：ad-admin\jeecg-boot\db\jeecgboot&ads_mysql5.7.sql
- 初始系统管理员： admin/123456


前端开发环境和依赖
----
- node
- yarn
- webpack
- eslint
- @vue/cli 3.2.1
- [ant-design-vue](https://github.com/vueComponent/ant-design-vue) - Ant Design Of Vue 实现
- [vue-cropper](https://github.com/xyxiao001/vue-cropper) - 头像裁剪组件
- [@antv/g2](https://antv.alipay.com/zh-cn/index.html) - Alipay AntV 数据可视化图表
- [Viser-vue](https://viserjs.github.io/docs.html#/viser/guide/installation)  - antv/g2 封装实现
- [jeecg-boot-angular 版本](https://gitee.com/dangzhenghui/jeecg-boot)


项目运行
----
1. 安装node.js
2. 切换到ads-admin\ant-design-jeecg-vue文件夹下
```
# 安装yarn
npm install -g yarn

# 下载依赖
yarn install

# 启动
yarn run serve

# 编译项目
yarn run build

# Lints and fixes files
yarn run lint
```
